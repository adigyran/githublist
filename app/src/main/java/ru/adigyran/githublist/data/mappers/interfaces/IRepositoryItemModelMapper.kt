package ru.adigyran.githublist.data.mappers.interfaces

import ru.adigyran.githublist.data.models.classes.RepositoryItemModel
import ru.adigyran.githublist.server.response.RepoResp
import ru.ivan.core.data.mappers.base.EntityMapper

abstract class IRepositoryItemModelMapper : EntityMapper<RepoResp,RepositoryItemModel>
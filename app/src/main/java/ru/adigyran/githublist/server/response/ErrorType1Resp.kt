package ru.adigyran.githublist.server.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorType1Resp(
    @Json(name = "error_code") val errorCode: String?,
    @Json(name = "errors") val errors: Map<String, List<String>>
)
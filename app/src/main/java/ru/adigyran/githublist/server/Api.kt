package ru.adigyran.githublist.server

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import ru.adigyran.githublist.server.response.RepoResp

interface Api {

    @GET("users/{username}/repos")
    fun getUserRepos(@Path("username") userName:String): Observable<List<RepoResp>>
}
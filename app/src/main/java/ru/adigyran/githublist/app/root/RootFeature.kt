package ru.adigyran.githublist.app.root

import android.content.Context
import com.badoo.mvicore.element.*
import io.reactivex.Observable
import ru.ivan.core.ext.observeOnMainThread
import ru.ivan.core.ui.BaseFeature
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorEvent
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter


class RootFeature(
    private val coordinatorRouter: CoordinatorRouter,
    context: Context
) :
    BaseFeature<RootFeature.Wish, RootFeature.Action, RootFeature.Effect, RootFeature.State, RootFeature.News>(
        initialState = State(),
        bootstrapper = BootstrapperImpl(),
        wishToAction = { Action.Execute(it) },
        actor = ActorImpl(coordinatorRouter),
        reducer = ReducerImpl(),
        postProcessor = PostProcessorImpl(),
        newsPublisher = NewsPublisherImpl(context)
    ) {
    init {

    }

    data class State(
        val stubParam: Boolean = false
    )

    sealed class Action {
        data class Execute(val wish: Wish) : Action()

    }
    sealed class Wish
    {

    }

    sealed class Effect
    {
        object EventSend : Effect()
    }

    class ActorImpl(
        private val coordinatorRouter: CoordinatorRouter
    ) : Actor<State, Action, Effect> {
        override fun invoke(state: State,  action: Action): Observable<Effect> = when (action) {
            is Action.Execute -> when (action.wish) {

                else -> Observable.empty<Effect>()
            }
           /* is Action.CheckAgreement -> {
                Observable.just(prefsManager.userAgreementCheck().get())
                    .doOnNext {
                        if(!it) coordinatorRouter.sendEvent(RootView.Events.OpenUserAgreement())
                    }
                    .map { Effect.EventSend as Effect }
                    .observeOnMainThread()
            }*/
            else -> Observable.empty()
        }
    }

    class ReducerImpl : Reducer<State, Effect> {
        override fun invoke(state: State, effect: Effect): State = when (effect) {
            is Effect.EventSend -> state.copy()
            else -> state.copy()
        }
    }

    sealed class News {
        data class Base(val news: BaseNews) : News()
    }

    class NewsPublisherImpl(val context: Context) : NewsPublisher<Action, Effect, State, News> {
        override fun invoke(action: Action, effect: Effect, state: State): News? = when (effect) {
            else -> null
        }
    }


    class BootstrapperImpl() : Bootstrapper<Action> {
        override fun invoke(): Observable<Action> = Observable.merge<Action>(
            listOf(

            )
        ).observeOnMainThread()
    }
    class PostProcessorImpl : PostProcessor<Action, Effect, State> {
        override fun invoke(action: Action, effect: Effect, state: State): Action? {
            return null
        }
    }

/*    @Subscribe
    fun onEvent(event: InvalidTokenEvent) = coordinatorRouter.sendEvent(Events.OpenLogin)

    @Subscribe
    fun onEvent(event: LogoutEvent) = coordinatorRouter.sendEvent(Events.OpenLogin)*/

    sealed class Events : CoordinatorEvent() {

    }


}
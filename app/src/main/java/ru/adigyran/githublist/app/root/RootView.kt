package ru.adigyran.githublist.app.root

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import com.badoo.mvicore.ModelWatcher
import com.badoo.mvicore.modelWatcher
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance
import org.kodein.di.generic.on
import ru.adigyran.githublist.R
import ru.adigyran.githublist.app.root.di.RootNavigatorParam
import ru.adigyran.githublist.app.root.di.rootContainerDiModule
import ru.adigyran.githublist.app.root.mvi.*
import ru.adigyran.githublist.databinding.ViewRootFragmentContainerBinding
import ru.adigyran.githublist.navigation.Screens

import ru.ivan.core.ext.inTransaction
import ru.ivan.core.managers.ActivityResult
import ru.ivan.core.ui.BindingsBase
import ru.ivan.core.ui.DiActivity
import ru.ivan.core.unclassifiedcommonmodels.navigation.BackButtonListener
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorEvent


class RootView :
    DiActivity<ViewRootFragmentContainerBinding, ViewModel, UiEvent, RootFeature.News>() {

    //region Инъекции

    // private val eventBus: EventBus by kodein.on(context = this).instance()

    private val coordinatorFactory: ((fragmentManager: FragmentManager) -> Coordinator) by kodein.on(
        context = this
    ).factory()

    private val navigatorFactory: ((param: RootNavigatorParam) -> RootNavigator) by kodein.on(
        context = this
    ).factory()

    private val bindingsFactory: ((lifecycleOwner: Lifecycle) -> Bindings) by kodein.on(context = this)
        .factory()
    //endregion

    //region MVI


    //endregion

    //region Перегруженные методы
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)




        if (savedInstanceState == null) {
            supportFragmentManager.inTransaction {
                replace(R.id.fragmentContainer, Screens.ReposListscreen().fragment)
            }
        }


    }


    override fun onPause() {
        localCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        localCicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onStart() {
        super.onStart()
        // eventBus.register(this)
    }

    override fun onStop() {
        super.onStop()
        //eventBus.unregister(this)
    }

    override fun onDestroy() {
        coordinatorHolder.removeCoordinator()
        super.onDestroy()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
        if ((fragment as? BackButtonListener)?.onBackPressed() == true) {
            return
        }
        localCicerone.router.exit()
    }

    override fun provideNavigator() =
        navigatorFactory(RootNavigatorParam(this, R.id.fragmentContainer))

    override fun provideCoordinator() = coordinatorFactory(supportFragmentManager)

    override fun provideModelWatcher(): ModelWatcher<ViewModel> = modelWatcher {
    }

    override fun provideBindings(): BindingsBase<ViewModel, UiEvent, RootFeature.News> =
        bindingsFactory(lifecycle)

    override fun provideDiModule() = rootContainerDiModule()

    override fun processNews(news: RootFeature.News) = when (news) {
        is RootFeature.News.Base -> processBaseNews(news.news)
    }

    override fun provideViewBinding(inflater: LayoutInflater): ViewRootFragmentContainerBinding =
        ViewRootFragmentContainerBinding.inflate(inflater)

    sealed class Events : CoordinatorEvent() {

        data class OpenUrl(val url: String) : Events()
        object Exit : Events()
        object ExitApp : Events()
    }


}
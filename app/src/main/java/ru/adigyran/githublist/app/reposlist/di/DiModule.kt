package ru.adigyran.githublist.app.reposlist.di

import androidx.lifecycle.Lifecycle
import org.kodein.di.Kodein
import org.kodein.di.generic.*
import ru.adigyran.githublist.app.reposlist.ReposListFeature
import ru.adigyran.githublist.app.reposlist.ReposListView
import ru.adigyran.githublist.app.reposlist.mvi.Bindings
import ru.ivan.core.di.scopes.CustomFragmentScope
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter



fun reposListDiModule(
    parentCoordinatorEvent: CoordinatorRouter,
    param: ReposListView.Param
) =
    Kodein.Module("reposListDiModule") {
        bind() from scoped(CustomFragmentScope).singleton { ReposListFeature(parentCoordinatorEvent, instance(),instance(),param) }
        bind() from factory { lifecycleOwner: Lifecycle -> Bindings(lifecycleOwner, instance(), instance()) }
    }
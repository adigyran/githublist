package ru.adigyran.githublist.app.root.di

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.*
import ru.adigyran.githublist.app.root.RootCoordinator
import ru.adigyran.githublist.app.root.RootFeature
import ru.adigyran.githublist.app.root.RootNavigator
import ru.adigyran.githublist.app.root.mvi.Bindings
import ru.ivan.core.di.scopes.CustomActivityScope

import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator



import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

fun rootContainerDiModule() = Kodein.Module("rootContainerDiModule") {
    bind<Coordinator>() with scoped(AndroidLifecycleScope.multiItem).multiton { fragmentManager: FragmentManager ->
        RootCoordinator(
            instance<Cicerone<Router>>().router,
            instance(),
            fragmentManager
        )
    }

    bind<RootNavigator>() with factory { param: RootNavigatorParam ->
        RootNavigator(param.activity, param.resId)
    }

    bind() from scoped(CustomActivityScope).singleton { RootFeature(instance(), instance()) }
    bind() from factory { lifecycleOwner: Lifecycle -> Bindings(lifecycleOwner, instance()) }
}

data class RootNavigatorParam(val activity: FragmentActivity, val resId: Int)
package ru.adigyran.githublist.navigation

import ru.adigyran.githublist.app.reposlist.ReposListView
import ru.ivan.core.ext.createFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    class ReposListscreen() : SupportAppScreen() {
        override fun getFragment() =
                createFragment<ReposListView, ReposListView.Param>(
                        ReposListView.Param()
                )
    }
}